FROM node:10-alpine

RUN mkdir -p /usr/local/app
WORKDIR /usr/local/app

COPY ./package.json /usr/local/app
COPY ./package-lock.json /usr/local/app
COPY ./config.js /usr/local/app
COPY ./index.js /usr/local/app
COPY ./style.css /usr/local/app
COPY ./resume.hbs /usr/local/app
COPY ./resume.json /usr/local/app

ADD ./public /usr/local/app/public

RUN npm i npm@latest -g && \
  npm install && \
  sed -i "s/localhost/0.0.0.0/g" node_modules/resume-cli/index.js node_modules/resume-cli/lib/serve.js

CMD ["npm", "start"]
