# Genocation JsonResume

Site with complete CV at genocation.xyz

## Contents

1. Resume generated with JSON resume template 
2. Footer with console credits


## Genocation theme 

A responsive theme for [JSON Resume](http://jsonresume.org/) with Bootstrap, custom thumbnails for each section, animated level bar charts and floating navigation menu.

Download it at its Gitlab repository [JSON Resume Genocation Theme](https://gitlab.com/genocation/jsonresume-theme-genocation)

## Installation

Install the npm dependencies

```
npm install
```

And generate and serve the resume from the json file `resume.json`
```
npm start
```

## Resume Docker container

To-Do
