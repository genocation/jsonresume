$.fn.console = function(options) {
   return new Console($(this), options);
}

function Console(element, options) {

   this.prompt = "root@127.0.0.1:~$ ";
   // Check options
   if (options) {
      if (options.prompt) {
         this.prompt = options.prompt+" ";
      }
   }

   this.screen = element;
   this.screen.addClass("cons");
   this.screen.html("<span class='display'>"+this.prompt+"</span><b class='prompter idle'>&marker;</b>");
   this.display = this.screen.find(".display");
   this.prompter = this.screen.find(".prompter");

   this.command = function(text, output, callback) {
      var that = this;
      var screen = this.screen;
      var prompter = this.prompter;
      var display = this.display;
      // Preparing to write this command
      text = text.replace(/\r\n?/g,'\n').split('');
      prompter.removeClass("idle");
      var typer = function() {
         var character = text.shift();
         display.append(character === '\n'?"<br>":character);
         // Calculating random idle between characters, from:
         // http://codereview.stackexchange.com/questions/97106/console-like-printing-of-message-javascript
         if (text.length) {
            var delay, next = text[0];
            //based on a querty pt-PT keyboard, there delays are subjective
            if (next.match(/[a-z\d\t\-\.,º]/)) {
               delay = 50;
            } else if(next == ' ' || next == '\n' || next.match(/[\\\|\!\"\#\$\%\&\/\(\)\=\?\'»\*ª_:;>A-Z]/)) {
               delay = 100;
            } else if(next.match(/[\@\€\£\§\{\[\]\}\~\´]/)) {
               delay = 150;
            } else {
               delay = 250;
            }
            if (next == character) {
               delay -= 25;
            }
            setTimeout(typer, delay + (Math.random() * 50));
         } 

         // We finished typing the command, we press enter
         else {
            display.append("<br>");
            prompter.addClass("idle");
            setTimeout(function() {
               that.output(output, callback);
            }, 1000);
         }
      };
      setTimeout(typer, 50 + (Math.random() * 50));
   };

   this.output = function(text, callback) {
      text = text.replace(/\r\n?/g,'\n').split('\n');
      for (let line of text) {
         this.display.append("<span class='output'>"+line+"</span><br>");
      }
      this.display.append(this.prompt);
      if (typeof callback === "function") {
         setTimeout(callback, 2000);
      }
   };


}

