jQuery(function ($) {

   var consoleScreen;
   var consoleVisible;
   var hideSpan = 200;
   var hideStart = $("#references").offset().top + $("#references").outerHeight() - hideSpan;

   var toggleConsole = function() {
      var offset = $(window).scrollTop();
      if (offset > hideStart) {
         if (offset < (hideStart + hideSpan)) {
            var opacity = 1 - ((offset-hideStart) / hideSpan);
            //$(".preconsole").css("opacity", opacity).show();
         } else {
            //$(".preconsole").css("opacity", 0).hide();
         }
      } else {
         //$(".preconsole").css("opacity", 1).show();
      }
   };

   var os = new OnScreen({
      tolerance: 0,
      debounce: 100,
      container: window
   });

   os.on('enter', '#skills', (element) => {
      $('#skills .progress-bar').removeClass("zero");
   });

   os.on('enter', '#languages', (element) => {
      $('#languages .progress-bar').removeClass("zero");
   });

   os.on('enter', "#console", (element) => {
      consoleVisible = true;
      if (!consoleScreen) {
         consoleScreen = $("#consoleScreen").console({
            prompt : "gengh@persephone:~$"
         });
         setTimeout(function() {
            startConsole(consoleScreen);
         }, 2000);
      }
   });

   os.on('leave', "#console", (element) => {
      consoleVisible = false;
   });

   $(window).scroll(function(e) {
      if (consoleVisible) {
         toggleConsole();
      }
   });

   $('#scroll-spy').on('activate.bs.scrollspy', function(e) {
      let currentSection = $(".nav li.active > a").attr("href");
      $("div.thumbnail.active").removeClass("active");
      $(currentSection+" div.thumbnail").addClass("active");
   });

   // Open floating navigation menu
   $(".flt-button a").on("click", function(e) {
      e.preventDefault();
      $("#flt-menu").slideDown(300);
   });

   $(".flt-close a").on("click", function(e) {
      e.preventDefault();
      $("#flt-menu").slideUp(300);
   });

   // Add smooth scrolling to all links inside a navbar
   $(".nav a").on('click', function(event){

      // Make sure this.hash has a value before overriding default behavior
      if (this.hash !== "") {

         // Prevent default anchor click behavior
         event.preventDefault();

         // Store hash (#)
         var hash = this.hash;

         // Close floating menu if it's open
         if ($("#flt-menu").length > 0) {
            $("#flt-menu").hide();
         }

         // Using jQuery's animate() method to add smooth page scroll
         // The optional number (800) specifies the number of milliseconds
         // it takes to scroll to the specified area (the speed of the animation)
         $('html, body').animate({
            scrollTop: $(hash).offset().top - 20
         }, 800, function(){

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
         }); // End if statement
      }
   });

   // Run
   toggleConsole();
});

function startConsole(console) {
// console.command(command, output, callback);
console.command("resume --info", `This resume is created following the JSON Resume Schema
More info at <a href="https://jsonresume.org" target="_blank">https://jsonresume.org/</a>
`, function() {
console.command("resume --theme", `Name:     Genocation
Author:   Gen GH
Version:  0.1.0
License:  <a href="https://opensource.org/licenses/MIT" target="_blank">MIT</a>
This theme is open and available at
<a href="https://gitlab.com/genocation/jsonresume-theme-genocation" target="_blank">https://gitlab.com/genocation/jsonresume-theme-genocation</a>
`, function() {
console.command("resume --help", `Usage
$ sudo npm install -g resume-cli
$ git clone https://gitlab.com/genocation/jsonresume-theme-genocation.git
$ cd jsonresume-theme-genocation
$ npm install
$ resume serve
`);
});
});
}


