{
	"basics": {
		"name": "Genoveva Galarza Heredero",
		"label": "Crafting Software",
		"picture": "",
		"email": "info@genocation.com",
		"website": "http://genocation.com",
		"summary": "I am a vocational programmer, a compulsive traveler and a passionate entrepreneur.<br>Being always interested in AI and Computational Linguistics, I spent four years in Hyderabad, India, working at Infosys Labs on the development of context-aware systems. This gave me the chance to dedicate a lot of my time to studying NLP solutions and applying those on smart search, relationship extraction, discourse resolution and human-computer interaction for speech-based agents.<br>In the meantime I developed an interest in the rise of Collaborative Consumption and its possitive effect on developing nations, which led me to co-found my first start-up, RideIT, a ridesharing service for companies which has won multiple Indian and international social entrepreneurship awards. Currently working with the Wikimedia Foundation to increase the availability of free knowledge online, with the main focus on those languages which are currently underrepresented online. <br>I love to code with the good guys.",
		"location": {
			"address": "Madrid, ESP",
			"postalCode": "",
			"city": "",
			"countryCode": "",
			"region": ""
		},
		"profiles": [{
				"network": "linkedin",
				"username": "www.linkedin.com/in/gengh",
				"url": "https://www.linkedin.com/in/gengh"
			},
			{
				"network": "gitlab",
				"username": "genocation",
				"url": "https://gitlab.com/genocation"
			},
			{
				"network": "keybase",
				"username": "genocation",
				"url": "https://keybase.io/genocation/"
			},
			{
				"network": "twitter",
				"username": "@genocation",
				"url": "https://twitter.com/genocation"
			},
			{
				"network": "blog",
				"username": "blog.genocation.com",
				"url": "https://blog.genocation.xyz"
			}
    ]
	},
	"work": [{
			"company": "Wikimedia Foundation",
			"position": "Software Engineer",
			"website": "https://wikimediafoundation.org/",
			"startDate": "2020-11-30",
			"summary": "The nonprofit Wikimedia Foundation provides the essential infrastructure for free knowledge. WMF hosts Wikipedia, the free online encyclopedia, created, edited, and verified by volunteers around the world, as well as many other vital community projects.",
			"highlights": [
				"Work on the development of Abstract Wikipedia, a project that aims to create a language-independent version of Wikipedia using structured data from Wikidata"
			]
		},
		{
			"company": "IUVIA",
			"position": "Founding Member and Developer",
			"website": "https://iuvia.io",
			"startDate": "2019-09-01",
			"endDate": "2020-09-01",
			"summary": "IUVIA is a privacy-by-design device and operating system that allows anyone to own their data, host their cloud services at home and enjoy all the operative benefits of the cloud without its privacy issues.",
			"highlights": [
				"Work on the design and development of the IUVIA Platform, a suite of software pieces to run and easily setup a self-hosting server.",
				"Help design and develop a protocol of self-contained and self-describing applications using AppImages.",
				"Develop the IUVIA Platform management interface using Django REST framework and Vue.",
				"Develop a product and a sustainable business model to regain ownership over our own data and end surveillance capitalism.",
        "Received funding as part NGI Ledger, an initiative under the European Union's Horizon 2020 research and innovation programme."
			]
		},
		{
			"company": "OpenShine",
			"position": "Software Engineer",
			"website": "http://www.openshine.com",
			"startDate": "2016-10-01",
			"endDate": "2019-08-14",
			"summary": "OpenShine is a Spanish company founded by a group of high qualified professionals with a well-known trajectory inside Open Source Software world. Its goal is to stay on top of the newest Big Data and Cloud technologies in order to build high quality solutions for our clients. OpenShine is driven by our love for good quality code, administrative transparency, good work ethics and an horizontal and colaborative structure.",
			"highlights": [
				"As a full stack developer, architect and build different projects using Django, Django REST framework, Vue and Angular.",
				"Provision and manage different infrastructures using container and cloud tools.",
				"Build fully automated CI/CD pipelines using gitlab-ci, Docker and Ansible.",
				"Build auto-scalable multi tenancy systems.",
				"Obtain Angular and Vue advanced skills by doing pretty much anything.",
				"Conceive, develop and mantain the biggest colaborative and Open Source platform for free webcomic hosting in Spanish: faneo.es."
			]
		},
		{
			"company": "RideIT",
			"position": "Co-Founder and CTO",
			"website": "https://youtu.be/wQFZiXc-0tg",
			"startDate": "2013-01-01",
			"endDate": "2016-09-30",
			"summary": "RideIT is an Indian startup operating on the urban transport sector. RideIT is an online multi-platform service that helps employees share their rides. RideIT offers a wide range of features to falicitate public or private vehicle sharing to our customers - as part of its B2C vertical -, and offers full transport solutions to companies - B2B. RideIT approaches the challenge of indian transport sharing by specializing on a route-based matching algorithm to maximize the relevance of search results, and collecting and analyzing information of the hourly rides happening in urban areas.",
			"highlights": [
				"Design and build a modular, scalable, efficient and reusable architecture for RideIT different features.",
				"Design and build the core route-matching algorithm.",
				"Manage the development team, build and maintain efficient and collaborative development in an international and plural team.",
				"Build and maintain a layered network architecture with zero downtime deployments using docker containers.",
				"Reache the consumer space, facilitating public and private vehicle sharing to more than 20,000 users, and provided eco-conscious transport solutions to companies as part of its B2B model."
			]
		},
		{
			"company": "Quarizmi AdTech",
			"position": "Data Scientist",
			"website": "https://www.quarizmi.com",
			"startDate": "2015-03-01",
			"endDate": "2015-09-30",
			"summary": "Quarizmi AdTech is an Advertising Technology company changing the way paid search is done. It applies Machine Learning and Natural Language Processing and Generation (NLP and NLG) to create highly profitable and scalable long tail campaigns that complement other PPC efforts, to bring an overall increase of up to ten-fold.",
			"highlights": [
				"Build software that uses different NLP techniques - statistical and symbolic - to analyze large sets of user-written queries and utterances.",
				"Build applications to help on keyword analysis, understand the possible user generated mistakes - grammar errors, abbreviations -, and generate trustworthy corrections.",
				"Build NLG tools to automatically generate relevant and engaging ads according to the AdWords constrains and definitions parting from very large sets of relevant keywords."
			]
		},
		{
			"company": "Infosys",
			"position": "Research Associate",
			"website": "https://www.infosys.com",
			"startDate": "2012-07-01",
			"endDate": "2013-12-31",
			"summary": "Worked at Infosys Labs - R&D unit of Infosys -, specializing on Natural Language Processing, Activity Context Aware Systems and Semantic Technologies for new generation digital workspaces.",
			"highlights": [
				"Designed and created different speech-based agents in different domains using Nuance Speech Recognition engines.",
				"Built discourse and semantic grammars and interaction FSMs and data models that would define the context and domain of the speech-based agents.",
				"Wrote, published and presented articles in diverse national and international AI conferences",
				"Mentored various interns coming as part of the InStep program"
			]
		},
		{
			"company": "Infosys",
			"position": "Junior Research Associate",
			"website": "https://www.infosys.com",
			"startDate": "2010-09-01",
			"endDate": "2012-07-30",
			"summary": "Worked at Infosys Labs - R&D unit of Infosys -, in the group C-KDIS - Centre for Knowledge-Driven Intelligent Systems - as a specialist in Natural Language Processing for Knowledge work Support Systems.",
			"highlights": [
				"Building the theoretical base of a novel class of knowledge management systems",
				"Exploring WordNet and NLP techniques in the frame of Information Extraction and Retrieval from unclassified and untagged texts.",
				"Create the architecture for a smart digital and context-aware workspace (NGDW)"
			]
		},
		{
			"company": "Applied Mathematics Department, Technical University of Madrid",
			"position": "Collaboration Scholar",
			"website": "http://www.dma.fi.upm.es/",
			"startDate": "2008-01-01",
			"endDate": "2008-09-01",
			"summary": "Developed different educational projects for the study of dynamic systems and fractal mathematics",
			"highlights": []
		}
	],
	"volunteer": [],
	"education": [{
			"institution": "Universidad Politécnica de Madrid (FI, UPM)",
			"area": "Computer Science",
			"studyType": "Engineering",
			"startDate": "2003-01-01",
			"endDate": "2008-01-31",
			"gpa": "7.05",
			"courses": []
		},
		{
			"institution": "Instituto Tecnológico de Buenos Aires (ITBA)",
			"area": "",
			"studyType": "Final Project",
			"startDate": "2008-01-01",
			"endDate": "2009-01-31",
			"gpa": "10",
			"courses": []
		}
	],
	"awards": [{
			"title": "Best Social Venture",
			"date": "2014-02-18",
			"awarder": "Microsoft Youth Spark Live",
			"summary": "RideIT awarded as best social venture at the global Microsoft YouthSpark tech business competition."
		},
		{
			"title": "Second Best Social Business",
			"date": "2014-03-21",
			"awarder": "APTE Summit, Ohio State University",
			"summary": "Participated in the APTE (Alleviating Poverty Through Entrepreneurship) 2014 Summit in Columbus, Ohio, and won the second prize at the APTE Social Business Competition."
		},
		{
			"title": "Best Go Green! Business",
			"date": "2014-12-01",
			"awarder": "World Summit Youth Award",
			"summary": "The World Summit Youth Award is a global contest which brings together young developers and digital entrepreneurs who use internet and mobile technology to put the UN Millennium Development Goals (MDGs) into action."
		},
		{
			"title": "Hottest Tech Start Up",
			"date": "2015-03-12",
			"awarder": "Hyderabad Software Enterprises Association",
			"summary": "RideIT won the Best Software Product Start-up at Consumer Early Stage, in the HYSEA Summit & Awards 2015, Hyderabad (India)"
		}
	],
	"publications": [{
			"name": "WordNet Context for Knowledge work Support Systems",
			"publisher": "ACM",
			"releaseDate": "2011-03-25",
			"website": "https://portal.acm.org/citation.cfm?id=1980435&CFID=31984521&CFTOKEN=91639288",
			"summary": ""
		},
		{
			"name": "Support Systems for Knowledge Works: A perspective of future of Knowledge Management Service",
			"publisher": "Proceedings of ICSEM 2011",
			"releaseDate": "2011-09-29",
			"website": "",
			"summary": ""
		},
		{
			"name": "Efficient Service Management in Healthcare Work: Harnessing Biomedical Linked Open Data",
			"publisher": "Proceedings of ICSEM 2011",
			"releaseDate": "2011-09-29",
			"website": "",
			"summary": ""
		},
		{
			"name": "Building Sustainable Healthcare Knowledge Systems by Harnessing Efficiencies from Biomedical Linked Open Data",
			"publisher": "Proceedings of INDICON 2011, Annual IEEE",
			"releaseDate": "2011-12-16",
			"website": "https://ieeexplore.ieee.org/xpl/freeabs_all.jsp?arnumber=6139343",
			"summary": ""
		},
		{
			"name": "Activity Context Aware Digital Workspaces and Consumer Playspaces: Manifesto and Architecture",
			"publisher": "AAAI'12 Toronto",
			"releaseDate": "2012-07-22",
			"website": "https://www.aaai.org/ocs/index.php/WS/AAAIW12/paper/view/5327",
			"summary": ""
		},
		{
			"name": "Activity Context-aware System Architecture for Intelligent Natural Speech Based Interfaces",
			"publisher": "AAAI'13 Bellevue",
			"releaseDate": "2013-07-14",
			"website": "https://www.aaai.org/ocs/index.php/WS/AAAIW13/paper/viewPaper/7179",
			"summary": ""
		}
	],
	"skills": [{
			"name": "Front End",
			"level": "Master",
			"keywords": [
				"JavaScript",
				"TypeScript",
				"ES6",
				"Angular",
				"Vue.js",
				"Webpack",
				"D3.js",
				"ECharts",
				"Highcharts",
				"Jest",
				"Karma"
			]
		},
		{
			"name": "Back End",
			"level": "Master",
			"keywords": [
				"Python",
				"Django",
				"Django REST framework",
				"Flask",
				"NodeJS",
				"J2EE",
				"PHP"
			]
		},
		{
			"name": "Sysadmin and DevOps",
			"level": "Advanced",
			"keywords": [
				"Linux",
				"Unix",
				"GCP",
				"AWS",
				"Docker",
				"Kubernetes",
				"Vagrant",
				"Gitlab-ci",
				"Ansible",
				"Google Professional Cloud Architect"
			]
		},
		{
			"name": "Computational Linguistics",
			"level": "Intermediate",
			"keywords": [
				"Stanford NLP",
				"NLTK",
				"WordNet",
				"FrameNet",
				"SRGS"
			]
		},
		{
			"name": "Other Languages",
			"level": "",
			"keywords": [
				"C++",
				"Haskell",
				"Prolog",
				"Java",
				"Bash"
			]
		},
		{
			"name": "Data",
			"level": "",
			"keywords": [
				"MySQL",
				"PostgreSQL",
				"MongoDB",
				"Cassandra",
				"ElasticSearch",
				"Redis",
				"RocksDB"
			]
		}
	],
	"languages": [{
			"language": "Spanish",
			"fluency": "Native"
		},
		{
			"language": "English",
			"fluency": "Master"
		},
		{
			"language": "Hindi",
			"fluency": "Intermediate"
		}
	],
	"interests": [{
			"name": "Art and Illustration",
			"keywords": [
				"Sketching and painting",
				"Free and open graphic tools",
				"Digital coloring with Krita",
				"Vector art with Inkscape",
				"20 years doing ASCII art"
			]
		},
		{
			"name": "Comic",
			"keywords": [
				"Comic reader",
				"Comic creator using Procreate",
				"Webcomic online culture",
				"Developer and maintainer of faneo.es"
			]
		},
		{
			"name": "Music",
			"keywords": [
				"Experimental music geek",
				"Flutist",
				"Collector and player of worldwide traditional flutes"
			]
		},
		{
			"name": "Nature",
			"keywords": [
				"Rural life lover",
				"Gardener and plant whisperer",
				"Mushroom forager and enthusiast",
				"Ecologist",
				"Vegetable grower"
			]
		}
	],
	"references": [{
			"name": "Iván Vallés Pérez",
			"reference": "It is my pleasure to recommend Genoveva. We worked together in Quarizmi AdTech and I was impressed with her deep knowledge in Natural Language Processing, Programming with Java, Redis Databases and Docker. She is a brave, very determined and, of course, a hugely skilled person with an admirable talent. I can say with confidence that she would be a very valuable worker for any team."
		},
		{
			"name": "Vikas Agrawal",
			"reference": "Genoveva came to us as part of the premier InStep program, and chose to join us full time. She went on to have first author publications, presentated her work on Activity Context-aware Systems at AAAI, mentored very successful interns, helped create discourse and semantic grammars for intelligent speech-enabled context-aware systems in multiple domains, and was always a jovial presence for the research team. Genoveva exhibited great leadership traits through her work with RideIT which was adopted by many corporations. She has great potential for further growth!"
		}
	]
}
